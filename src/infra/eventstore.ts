import { EventStore } from '../application/di';
import type { Event } from '../domain/events/event'

export const makeEventStore = () => new MemoryEventStore();

class MemoryEventStore implements EventStore {
  private eventStore = new Map<string, Event[]>();

  saveEvents(aggregateId: string, events: Event[]) {
    if (!this.eventStore.has(aggregateId)) this.eventStore.set(aggregateId, []);

    (this.eventStore.get(aggregateId) ?? []).push(...events);
  }

  getEvents(aggregateId: string) {
    return this.eventStore.get(aggregateId) ?? [];
  }
}