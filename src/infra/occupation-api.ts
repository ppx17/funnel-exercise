import { OccupationApi } from "../application/di";


const data = {
  '12': {
    occupationCode: '12',
    occupation: 'Hairdresser',
    sector: 'Beauty'
  },
  '13': {
    occupationCode: '13',
    occupation: 'Painter',
    sector: 'Construction'
  },
  '14': {
    occupationCode: '14',
    occupation: 'Carpenter',
    sector: 'Construction'
  }
} as const;

export class TestOccupationApi implements OccupationApi {

  getOccupation(occupationCode: string): { occupationCode: string; occupation: string; sector: string; } | undefined {
    return occupationCode in data ? data[occupationCode as keyof typeof data] : undefined;
  }
}