import type { Event } from '../domain/events/event'
import { makeEventStore } from '../infra/eventstore';
import { TestOccupationApi } from '../infra/occupation-api';

export interface EventStore {
  saveEvents(aggregateId: string, events: Event[]): void;
  getEvents(aggregateId: string): Event[]
}

export interface OccupationApi {
  getOccupation(occupationCode: string): { occupationCode: string, occupation: string, sector: string } | undefined;
}

export type Dependencies = {
  eventStore: EventStore;
  occupationApi: OccupationApi

}

const diContainer = (): Dependencies => ({
  eventStore: makeEventStore(),
  occupationApi: new TestOccupationApi()
})