
type ApiGatewayRequest = {
  method: 'POST' | 'GET';
  body: string
}

type ApiGatewayResponse = {
  statusCode: number;
  body: string;
}


export const commandHandler = (_request: ApiGatewayRequest): ApiGatewayResponse => {

  return { statusCode: 200, body: '' }
}